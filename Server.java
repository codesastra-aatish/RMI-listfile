package practice.listfile;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import java.io.File;

public class Server implements ListFile{

	public Server(){

	}

	public File[] listFile(){
		File[] files = new File(".").listFiles();

		return files;
	}

	public static void main(String args[]) {
		try{
			Server obj = new Server();

			ListFile stub = (ListFile) UnicastRemoteObject.exportObject(obj, 0);

			Registry registry = LocateRegistry.getRegistry();
			registry.bind("ListFile", stub);

			System.err.println("Server ready");
		}catch(Exception e){
			System.err.print("Server exception: "+e.toString());
			e.printStackTrace();
		}
	}
}
