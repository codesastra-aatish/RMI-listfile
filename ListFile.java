package practice.listfile;

import java.rmi.Remote;
import java.rmi.RemoteException;

import java.io.File;


public interface ListFile extends Remote{
	File[] listFile() throws RemoteException;
}
