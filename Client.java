package practice.listfile;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import java.io.File;

public class Client{

	private Client(){

	}

	public static void main(String args[]) {

		String host = (args.length < 1) ? null : args[0];

		try{
			Registry registry = LocateRegistry.getRegistry(host);

			ListFile stub = (ListFile) registry.lookup("ListFile");
			File[] response = stub.listFile();

			for (File file : response) {
				if (file.isFile()) {
					System.out.println(file.getName());
				}
			}

		}catch(Exception e){
			System.err.println("Client exception"+e.toString());
			e.printStackTrace();
		}
	}
}
